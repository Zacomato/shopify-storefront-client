import Vue from 'vue'
import Vuex from 'vuex'
// import _ from 'lodash'

Vue.use(Vuex)

const state = {
  isCartOpened: false
}

export default new Vuex.Store({
  state
})
